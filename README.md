SF Maps with ReactJS and D3JS
-------------

Modular reactjs project showing SF buses in a map(updating every 15s),
built it with D3JS.


----------


Develop
-------------

> **Steps:**

> 1. Has installed NodeJS
> 2. Clone the repository
> 3. yarn or npm install
> 4. npm run dev
> 5. Running on http://localhost:3000/



Build
-------------------

> **Steps:**

> 1. Repeat 1 to 3 steps of develop
> 2. npm run build
> 3. In ./dist are static files for production

