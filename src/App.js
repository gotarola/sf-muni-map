import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import helpers from './Helpers';
import Map from './Components/Map';
import Selector from './Components/Selector';
import styles from './App.css';

const { processBusesResult, initConfig } = helpers;

const {
  intervalTime,
  routesRequest,
  busesLocationsRequest,
} = initConfig();

let INTERVAL_REQUEST;

class App extends Component {
  constructor() {
    super();
    this.state = {
      routes: {},
      buses: {},
      routesSelected: [],
      isLoading: true,
    };
  }

  /**
   * Before render, get routes and buses
   */
  componentWillMount() {
    Promise.all([routesRequest.get(), busesLocationsRequest.get()])
      .then(([routesRes, busesRes]) => {
        const { data: routes } = routesRes;
        const { data: buses } = busesRes;

        this.setState({
          buses,
          routes,
          isLoading: false,
        });
      })
      .catch((error) => {
        // TODO: hanlder error
        // eslint-disable-next-line
        console.log(error);
      });
  }

  /**
   * After render, set an interval for update every 15 seconds
   */
  componentDidMount() {
    const repeatFn = () => {
      Promise.all([busesLocationsRequest.get()])
        .then((results) => {
          const buses = processBusesResult(results);

          this.setState({
            buses,
          });
        })
        .catch((error) => {
          // TODO: hanlder error
          // eslint-disable-next-line
          console.log(error);
        });
    };

    INTERVAL_REQUEST = setInterval(repeatFn, intervalTime);
  }

  /**
   * If user select some route/s update buses request params and interval requests
   * @param routesArray
   */
  selectRoute = (routesArray) => {
    const routesSelected = [].concat(routesArray);

    this.setState({
      routesSelected,
      isLoading: true,
    }, () => {
      const repeatFn = () => {
        clearInterval(INTERVAL_REQUEST);

        let arrayRequest;

        const promises = routesSelected.map((route) => {
          const { value: r } = route;

          return busesLocationsRequest.get({ r });
        });

        if (promises.length > 0) {
          arrayRequest = promises;
        } else {
          arrayRequest = [busesLocationsRequest.get()];
        }

        Promise.all(arrayRequest)
          .then((results) => {
            const buses = processBusesResult(results);

            this.setState({
              buses,
              isLoading: false,
            });

            INTERVAL_REQUEST = setInterval(repeatFn, intervalTime);
          })
          .catch((error) => {
            // TODO: hanlder error
            // eslint-disable-next-line
            console.log(error);
          });
      };

      repeatFn();
    });
  };

  render() {
    const {
      buses,
      routes,
      isLoading,
      routesSelected,
    } = this.state;

    return (
      <div className={styles.mainContainer}>
        <Selector
          routes={routes}
          loading={isLoading}
          values={routesSelected}
          select={this.selectRoute}
        />
        <Map
          buses={buses}
          loading={isLoading}
          routesSelected={routesSelected}
        />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
