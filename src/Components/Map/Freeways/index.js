import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

class Freeways extends Component {
  /**
   * Get main content for freeways
   */
  getFreewaysDraw = () => {
    const {
      path,
      data,
    } = this.props;

    const { features } = data;

    return features.map((feature, index) => (
      <path
        key={`freeway-${index}`}
        className={styles.freeways}
        d={path(feature)}
      />
    ));
  };


  render() {
    const freewaysPaths = this.getFreewaysDraw();

    return (
      <g>
        {freewaysPaths}
      </g>
    );
  }
}

Freeways.propTypes = {
  path: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default Freeways;
