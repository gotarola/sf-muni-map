import * as d3 from 'd3';

import streets from '../../assets/sfmaps/streets.json';
import arteries from '../../assets/sfmaps/arteries.json';
import freeways from '../../assets/sfmaps/freeways.json';
import neighborhoods from '../../assets/sfmaps/neighborhoods.json';

const getD3Config = (width, height, mainData) => {
  const projection = d3.geoMercator()
    .fitSize([width, height], mainData);

  const path = d3.geoPath()
    .projection(projection);

  return {
    path,
    width,
    height,
    projection,
  };
};

const getInitGeojson = () => ({
  streets,
  freeways,
  arteries,
  neighborhoods,
});

export default {
  getD3Config,
  getInitGeojson,
};
