import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

class Arteries extends Component {
  /**
   * Get main content for arteries
   */
  getArteriesDraw = () => {
    const {
      path,
      data,
    } = this.props;

    const { features } = data;

    return features.map((feature, index) => (
      <path
        key={`arterie-${index}`}
        className={styles.arteries}
        d={path(feature)}
      />
    ));
  };


  render() {
    const arteriesPath = this.getArteriesDraw();

    return (
      <g>
        {arteriesPath}
      </g>
    );
  }
}

Arteries.propTypes = {
  path: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default Arteries;
