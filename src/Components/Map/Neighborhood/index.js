import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

class Neighborhoods extends Component {
  /**
   * Get main content for neighborhoods
   */
  getNeighborhoodsDraw = () => {
    const {
      path,
      data,
    } = this.props;

    const { features } = data;

    return features.map((feature, index) => (
      <path
        key={`neighborhood-${index}`}
        className={styles.neighborhood}
        d={path(feature)}
      />
    ));
  };


  render() {
    const nbhPaths = this.getNeighborhoodsDraw();

    return (
      <g>
        {nbhPaths}
      </g>
    );
  }
}

Neighborhoods.propTypes = {
  path: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default Neighborhoods;
