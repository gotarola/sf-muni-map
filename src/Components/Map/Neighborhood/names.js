import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

class NeighborhoodsNames extends Component {
  /**
   * Get main content for neighborhoods names
   */
  getNames = () => {
    const {
      path,
      data,
    } = this.props;

    const { features } = data;

    return features.map((feature, index) => {
      const { properties } = feature;

      const [x, y] = path.centroid(feature);

      return (
        <text
          key={`neighborhood-${index}`}
          className={styles.neighborhoodName}
          x={x}
          y={y}
        >
          {properties.neighborho}
        </text>
      );
    });
  };


  render() {
    const namesContent = this.getNames();

    return (
      <g>
        {namesContent}
      </g>
    );
  }
}

NeighborhoodsNames.propTypes = {
  path: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default NeighborhoodsNames;
