import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

const BUS_ICON = '\uf207';

class Buses extends Component {
  /**
   * Get main content for buses
   */
  getContentBuses = () => {
    const {
      buses,
      projection,
    } = this.props;

    const { vehicle } = buses;

    return vehicle.map((bus, index) => {
      const { id, lat, lon } = bus;

      const [x, y] = projection([lon, lat]);

      return (
        <g key={`bus-${id}-${index}`}
           transform={`translate(${x}, ${y})`}
        >
          <text className={styles.busIcon}>
            {BUS_ICON}
            </text>
        </g>
      );
    });
  };

  render() {
    const busesContent = this.getContentBuses();

    return (
      <g>
        {busesContent}
      </g>
    );
  }
}

Buses.propTypes = {
  buses: PropTypes.object.isRequired,
  projection: PropTypes.func.isRequired,
};

export default Buses;
