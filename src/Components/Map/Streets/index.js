import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

class Streets extends Component {
  /**
   * Get main content for streets
   */
  getStreetsDraw = () => {
    const {
      path,
      data,
    } = this.props;

    const { features } = data;

    return features.map((feature, index) => (
      <path
        key={`street-${index}`}
        className={styles.streets}
        d={path(feature)}
      />
    ));
  };


  render() {
    const streetsPaths = this.getStreetsDraw();

    return (
      <g>
        {streetsPaths}
      </g>
    );
  }
}

Streets.propTypes = {
  path: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default Streets;
