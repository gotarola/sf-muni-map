import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Buses from './Buses';
import Streets from './Streets';
import Spinner from '../Spinner';
import Arteries from './Arteries';
import Freeways from './Freeways';
import Neighborhoods from './Neighborhood';
import NeighborhoodsNames from './Neighborhood/names';

import helpers from './helpers';

import styles from './styles.css';

const {
  getD3Config,
  getInitGeojson,
} = helpers;

const {
  streets,
  arteries,
  freeways,
  neighborhoods,
} = getInitGeojson();

class Map extends Component {
  constructor() {
    super();

    this.state = {
      path: {},
      width: 0,
      buses: {},
      height: 0,
      projection: {},
    };
  }

  /**
   * Update buses data with ajax call is resolved (first render)
   */
  componentWillMount() {
    const { buses } = this.props;

    this.setState({
      buses,
    });
  }

  /**
   * Update this.state.buses data every 15 seconds
   * @param newProps
   */
  componentWillReceiveProps(newProps) {
    const { buses } = newProps;

    this.setState({
      buses,
    });
  }

  /**
   * Listen for browser resize
   * Set main projection and geoPath
   * Set client height and width
   */
  componentDidMount() {
    window.addEventListener('resize', this.getClientDimensions);

    const { offsetWidth, offsetHeight } = document.getElementById('mapContainer');

    const {
      path,
      width,
      height,
      projection,
    } = getD3Config(offsetWidth, offsetHeight, neighborhoods);

    this.setState({
      path,
      width,
      height,
      projection,
    });
  }

  /**
   * If client screen size change, set new paremeters
   */
  getClientDimensions = () => {
    const { offsetWidth, offsetHeight } = document.getElementById('mapContainer');

    const {
      path,
      width,
      height,
      projection,
    } = getD3Config(offsetWidth, offsetHeight, neighborhoods);

    this.setState({
      path,
      width,
      height,
      projection,
    });
  };

  /**
   * Get main content for render
   * @returns {XML}
   */
  getMapContent = () => {
    const {
      path,
      buses,
      width,
      height,
      projection,
    } = this.state;

    const { loading } = this.props;

    if (!loading) {
      return (
        <svg width={width} height={height}>
          <g>
            <Neighborhoods
              path={path}
              data={neighborhoods}
            />
            <Streets
              path={path}
              data={streets}
            />
            <Arteries
              path={path}
              data={arteries}
            />
            <Freeways
              path={path}
              data={freeways}
            />
            <NeighborhoodsNames
              path={path}
              data={neighborhoods}
            />
          </g>
          <Buses
            buses={buses}
            projection={projection}
          />
        </svg>
      );
    }

    return (
      <Spinner />
    );
  };

  getInformation = () => {
    const { buses } = this.state;

    if (buses && buses.lastTime) {
      const { vehicle } = buses;
      const ts = parseInt(buses.lastTime.time, 10);
      const date = moment(ts);

      return (
        <div className={styles.informationContainer}>
          <h4>
            {`Last update: ${date.format('MM/DD/YYYY H:mm a')}`}
          </h4>
          <h4>
            {`Total buses: ${vehicle.length}`}
          </h4>
        </div>
      );
    }

    return (
      <div className={styles.informationContainer}>
        <h4>
          Loading...
        </h4>
      </div>
    );
  };

  render() {
    const mapContent = this.getMapContent();
    const informationContent = this.getInformation();

    return (
      <div id="mapContainer" className={styles.mapContainer}>
        {informationContent}
        {mapContent}
      </div>
    );
  }
}

Map.propTypes = {
  buses: PropTypes.shape({
    vehicle: PropTypes.array,
    lastTime: PropTypes.shape({
      time: PropTypes.string,
    }),
  }),
  routesSelected: PropTypes.array,
  loading: PropTypes.bool.isRequired,
};

export default Map;
