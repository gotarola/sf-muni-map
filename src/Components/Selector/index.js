import React, { Component } from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

import 'react-select/dist/react-select.css';

class Selector extends Component {
  constructor() {
    super();
    this.state = {};
  }

  /**
   * Construct route selector
   * @returns {Array}
   */
  getRoutes = () => {
    const { routes } = this.props;
    const { route } = routes;

    if (route) {
      return route.map((_route) => {
        const {
          tag: value,
          title: label,
        } = _route;

        return {
          label,
          value,
        };
      });
    }

    return [];
  };

  render() {
    const { loading, values, select } = this.props;
    const routes = this.getRoutes();

    return (
      <div className="col-xs-6">
        <Select
          value={values}
          multi={true}
          placeholder="Select some route/s"
          options={routes}
          onChange={select}
          disabled={loading}
        />
      </div>
    );
  }
}

Selector.propTypes = {
  routes: PropTypes.shape({
    route: PropTypes.array,
  }).isRequired,
  select: PropTypes.func.isRequired,
  values: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default Selector;
