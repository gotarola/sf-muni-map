import React, { Component } from 'react';

import styles from './styles.css';

class Spinner extends Component {
  render() {
    return (
      <div className={styles.spinnerContainer}>
        <i className="fa fa-5x fa-spinner fa-spin"/>
      </div>
    );
  }
}

export default Spinner;
