import _ from 'lodash';
import axios from 'axios';

const INTERVAL_TIME = 15000;
const NB_URL = 'http://webservices.nextbus.com/service/publicJSONFeed';

const ROUTES_INIT_PARAMS = {
  command: 'routeList',
  a: 'sf-muni',
};

const BUSES_LOCATIONS_INIT_PARAMS = {
  command: 'vehicleLocations',
  a: 'sf-muni',
  t: 0,
};

/**
 * Create new request with init and common data
 * @param _params
 * @returns {{get: (function(*=): AxiosPromise)}}
 */
const getRequest = (_params) => {
  const params = _params;

  return {
    get: (newParams) => (
      axios({
        params: _.assign({}, params, newParams),
        method: 'get',
        url: NB_URL,
      })
    ),
  };
};

/**
 * Get data from Promise.all results
 * @param results
 * @returns {{lastTime: {time: string}}}
 */
const processBusesResult = (results) => {
  const buses = {
    lastTime: {
      time: (new Date().getTime()).toString(),
    },
  };

  let vehicles = [];

  results.forEach((result) => {
    const { vehicle } = result.data;

    if (vehicle) {
      vehicles = vehicles.concat(vehicle);
    }
  });

  buses.vehicle = vehicles;

  return buses;
};

const initConfig = () => ({
  intervalTime: INTERVAL_TIME,
  routesRequest: getRequest(ROUTES_INIT_PARAMS),
  busesLocationsRequest: getRequest(BUSES_LOCATIONS_INIT_PARAMS),
});

export default {
  initConfig,
  processBusesResult,
};
